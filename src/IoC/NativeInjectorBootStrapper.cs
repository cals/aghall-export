﻿using AAE.Caching;
using CALS.AgHallExport.Application.GGTool;
using CALS.AgHallExport.Data.Factories;
using CALS.AgHallExport.Infrastructure.Data.Queries;
using CALS.AgHallExport.Interfaces;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace CALS.AgHallExport
{
    public static class NativeInjectorBootStrapper
    {
        /// <summary>
        /// Add services for the application to the specified <see cref="IServiceCollection"/>.
        /// </summary>
        /// <param name="services"></param>
        /// <param name="configure"></param>
        public static IServiceCollection AddAppServices(this IServiceCollection services, Action<AppOptions> configure)
        {
            if (services == null)
                throw new ArgumentNullException(nameof(services));

            if (configure == null)
                throw new ArgumentNullException(nameof(configure));

            var options = new AppOptions();
            configure(options);

            services.AddTransient(o => options);

            services.AddAppServices(connectionString: options.ConnectionString);

            return services;

        }

        /// <summary>
        /// Adds all application services to the specified <see cref="IServiceCollection"/>.
        /// </summary>
        /// <param name="services"></param>
        /// <param name="connectionString"></param>
        public static IServiceCollection AddAppServices(this IServiceCollection services, string connectionString)
        {
            services.AddTransient<IGGToolImportCreator, GGToolImportCreator>();

            // ############# CONTEXTS ################

            // ############# QUERIES ################
            services.AddTransient<IEpmExportQueries, EpmExportQueries>();

            // ############# REPOSITORIES ################

            // ############# STORES ################

            // ############# SERVICES ################

            // ############# CONNECTION FACTORIES ################
            services.AddSingleton<IDbConnectionFactoryEpm, DbConnectionFactoryEpm>(c => new DbConnectionFactoryEpm(connectionString));

            services.AddInMemoryCaching();

            return services;
        }
    }
}
