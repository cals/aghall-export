namespace CALS.AgHallExport
{
    public class AppOptions
    {
        /// <summary>
        /// Connection string for AgHallExport database
        /// </summary>
        public string ConnectionString { get; set; }
    }
}
