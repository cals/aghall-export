﻿using System;
using System.Threading.Tasks;
using AAE.Helpers.Email;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Mvc;

namespace AAE.Helpers.Errors
{

    public class ErrorController : Controller
    {
        private readonly IEmailSender _emailSender;

        public ErrorController(IEmailSender emailSender)
        {
            _emailSender = emailSender;
        }

        [Route("~/error")]
        [Route("~/error/{statusCode:int}")]
        [AllowAnonymous]
        public async Task<IActionResult> Error(int? statusCode = null)
        {
            var model = new ErrorModel();

            if (statusCode == null)
                statusCode = HttpContext.Response.StatusCode;

            model.StatusCode = statusCode.Value;

            var feature = HttpContext.Features.Get<IStatusCodeReExecuteFeature>();

            // get routing data
            var endpointModel = EndpointExtractor.Extract(HttpContext);

            var request = HttpContext.Request;

            var errorModel = new ErrorEmailModel()
            {
                Query = request.Query,
                Cookies = request.Cookies,
                Headers = request.Headers,
                RouteValues = request.RouteValues,
                Endpoint = endpointModel,
                StatusCode = statusCode.Value,
                IpAddress = HttpContext.Connection.RemoteIpAddress.ToString(),
                UserId = "Unknown",
                Uri = new Uri($"{HttpContext.Request.Scheme}://{HttpContext.Request.Host}{feature?.OriginalPath}")
            };

            //string uaString = context.Request.Headers["User-Agent"].ToString();
            if (HttpContext.User.Identity.IsAuthenticated)
            {
                errorModel.UserId = HttpContext.User.Identity.Name;
            }

            var emailBody = ErrorEmailCreator.CreateStatusCodeEmail(errorModel);


            if (model.StatusCode != 500)
            {
                await _emailSender.SendAdminEmailAsync($"{statusCode.Value} Error: {errorModel.Uri}", emailBody);
            }


            return View(model);
        }

    }
}
