﻿using System;
using System.Threading.Tasks;
using CALS.AgHallExport.Interfaces;
using ClosedXML.Excel;
using DocumentFormat.OpenXml.Spreadsheet;
using Microsoft.AspNetCore.Mvc;

namespace CALS.AgHallExport.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly IEpmExportQueries _epmExportQueries;
        private readonly IGGToolImportCreator _ggToolImport;

        public HomeController(IEpmExportQueries epmExportQueries, IGGToolImportCreator ggToolImport)
        {
            _ggToolImport = ggToolImport ?? throw new ArgumentNullException(nameof(ggToolImport));
            _epmExportQueries = epmExportQueries ?? throw new ArgumentNullException(nameof(epmExportQueries));
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpGet("export")]
        public async Task<IActionResult> Export()
        {

            // Response...
            System.Net.Mime.ContentDisposition cd = new System.Net.Mime.ContentDisposition
            {
                FileName = $"AgHall_{DateTime.Now:yyyyMMdd}.csv",
                Inline = false  // false = prompt the user for downloading;  true = browser to try to show the file inline
            };
            Response.Headers.Add("Content-Disposition", cd.ToString());
            Response.Headers.Add("X-Content-Type-Options", "nosniff");

            var members = await _epmExportQueries.GetEmailAsync().ConfigureAwait(false);

            var csv = await _ggToolImport.CreateCSV(members).ConfigureAwait(false);

            return File(csv, "text/csv");

        }

        [HttpGet("directory")]
        public async Task<IActionResult> Directory()
        {

            // Response...
            System.Net.Mime.ContentDisposition cd = new System.Net.Mime.ContentDisposition
            {
                FileName = $"AgHallDirectory_{DateTime.Now:yyyyMMdd}.csv",
                Inline = false  // false = prompt the user for downloading;  true = browser to try to show the file inline
            };
            Response.Headers.Add("Content-Disposition", cd.ToString());
            Response.Headers.Add("X-Content-Type-Options", "nosniff");

            var members = await _epmExportQueries.GetDirectoryAsync().ConfigureAwait(false);


            using (var workbook = new XLWorkbook())
            {
                var worksheet = workbook.Worksheets.Add("Occupants");
                var currentRow = 1;
                worksheet.Cell(currentRow, 1).Value = "Name";
                worksheet.Cell(currentRow, 2).Value = "Dept";
                foreach (var member in members)
                {
                    currentRow++;
                    worksheet.Cell(currentRow, 1).Value = member.Name;
                    worksheet.Cell(currentRow, 2).Value = member.Department;
                }

                using (var stream = new MemoryStream())
                {
                    workbook.SaveAs(stream);
                    var content = stream.ToArray();

                    return File(
                        content,
                        "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                        $"AgHallDirectory_{DateTime.Now:yyyyMMdd}.xlsx");
                }
            }

        }
    }
}

