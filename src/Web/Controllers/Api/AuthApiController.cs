﻿using CALS.AgHallExport.Identity;
using CALS.AgHallExport.Web.Security;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CALS.AgHallExport.Web.Controllers.Api
{
    [Route("api/auth")]
    [ApiController]
    public class AuthApiController : ControllerBase
    {
        private readonly AppGroups _appGroups;
        public AuthApiController(AppGroups appGroups)
        {
            _appGroups = appGroups;
        }

        // GET: api/auth/groups
        /// <summary>
        /// Returns a listing of all system groups, and all groups that the logged-in user is a member of
        /// </summary>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [HttpGet("groups", Name = "Auth_Groups")]
        public IActionResult GetGroups()
        {
            var user = AgHallExportUser.UserFromPrincipal(HttpContext.User);

            return Ok(new { system = _appGroups, user = user.Groups });
        }
    }
}
