﻿using System;
using System.Threading.Tasks;
using CALS.AgHallExport.Identity;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace CALS.AgHallExport.Web.Identity
{
    /// <summary>
    /// Middleware to add this app user's identity to the ClaimsPrincipal
    /// </summary>
    public class IdentityMiddleware
    {
        private readonly IdentityUserOptions _options;
        private readonly RequestDelegate _next;
        private readonly ILogger _logger;

        /// <summary>
        /// Creates a new instance of the <see cref="IdentityMiddleware"/>.
        /// </summary>
        /// <param name="next">The next middleware in the pipeline.</param>
        /// <param name="options">The configuration options.</param>
        /// <param name="loggerFactory">An <see cref="ILoggerFactory"/> instance used to create loggers.</param>
        public IdentityMiddleware(RequestDelegate next, IOptions<IdentityUserOptions> options, ILoggerFactory loggerFactory)
        {
            if (options == null)
            {
                throw new ArgumentNullException(nameof(options));
            }

            if (loggerFactory == null)
            {
                throw new ArgumentNullException(nameof(loggerFactory));
            }


            _next = next ?? throw new ArgumentNullException(nameof(next));
            _options = options.Value;
            _logger = loggerFactory.CreateLogger<IdentityMiddleware>();
        }


        /// <summary>
        /// Processes a request to add the AgHallExport identity to the user principal
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public async Task Invoke(HttpContext context)
        {

            // create the new AgHallExport identity to add to the existing ClaimsPrincipal
            var user = new AgHallExportUser(context.User);


            context.User.AddIdentity(user.ToClaimsIdentity());

            await _next(context);
        }
    }
}
