﻿using System;
using System.Linq;
using System.Security.Claims;
using AAE.Authentication;

namespace CALS.AgHallExport.Identity
{
    /// <summary>
    /// <see cref="IApplicationUser"/> for the Directory Edit app
    /// </summary>
    public class AgHallExportUser : ClaimsApplicationUser, IApplicationUser
    {
        public const string AgHallExportAuthenticationType = "AgHallExport";

        public AgHallExportUser(ClaimsIdentity identity) : base(identity)
        {

        }

        public AgHallExportUser(ClaimsPrincipal principal) : this((ClaimsIdentity)principal.Identity)
        {

        }

        public override ClaimsIdentity ToClaimsIdentity(string authenticationType = null)
        {
            var ident = base.ToClaimsIdentity(AgHallExportAuthenticationType);
            return ident;
        }

        /// <summary>
        /// Returns a <see cref="AgHallExportUser"/> populated from the proper identity in the supplied <see cref="ClaimsPrincipal"/>
        /// </summary>
        ///<remarks>Throws an <see cref="Exception"/> if the proper identity is not in the <see cref="ClaimsPrincipal"/></remarks>
        public static AgHallExportUser UserFromPrincipal(ClaimsPrincipal claimsPrincipal)
        {
            var appIdentity = claimsPrincipal.Identities.Where(i => i.AuthenticationType == AgHallExportAuthenticationType).FirstOrDefault();
            if (appIdentity == null)
                throw new Exception("No AgHallExport identity found in the supplied ClaimsPrincipal");

            return new AgHallExportUser(appIdentity);
        }
    }
}
