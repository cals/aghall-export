﻿using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Options;

namespace CALS.AgHallExport.Web.Identity
{
    public static class IApplicationBuildingExtensions
    {
        /// <summary>
        /// Enables use of the AgHallExport user identity
        /// </summary>
        /// <param name="app"></param>
        /// <returns></returns>
        public static IApplicationBuilder UseAgHallExportIdentity(this IApplicationBuilder app)
        {
            if (app == null)
            {
                throw new ArgumentNullException(nameof(app));
            }

            return app.UseMiddleware<IdentityMiddleware>();
        }

        /// <summary>
        /// Enables AgHallExport user with the given options
        /// </summary>
        public static IApplicationBuilder UseAgHallExportIdentity(this IApplicationBuilder app, IdentityUserOptions options)
        {
            if (app == null)
            {
                throw new ArgumentNullException(nameof(app));
            }

            if (options == null)
            {
                throw new ArgumentNullException(nameof(options));
            }

            return app.UseMiddleware<IdentityMiddleware>(Options.Create(options));
        }
    }
}
