﻿namespace CALS.AgHallExport.Web.ViewModels
{
    /// <summary>
    /// View model used for the main menu
    /// </summary>
    public class MainMenu
    {
        public string LoggedInUser { get; set; }
    }
}
