﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace CALS.AgHallExport.Web.Helpers
{
    /// <summary>
    /// Action filter to inject app config from appsettings into views
    /// </summary>
    public class AppConfigActionFilter : IAsyncActionFilter
    {
        private AppConfig _appConfig;

        public AppConfigActionFilter(AppConfig config)
        {
            _appConfig = config;
        }

        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            if (context.Controller is Controller)
                ((Controller)context.Controller).ViewBag.AppConfig = _appConfig;
            await next();
        }
    }
}
