﻿using AAE.Helpers.Email;
using CALS.AgHallExport.Web.Authorization;

namespace CALS.AgHallExport.Web.Helpers
{
    /// <summary>
    /// Class to hold app config options from appsettings
    /// </summary>
    public class AppConfig
    {
        public AppConfig()
        {
            EmailSettings = new EmailConfig();
            Groups = new AppGroupConfig();
        }
        public string Branding { get; set; }
        public string JWTSecret { get; set; }
        public EmailConfig EmailSettings { get; set; }
        public AppGroupConfig Groups { get; set; }
    }
}
