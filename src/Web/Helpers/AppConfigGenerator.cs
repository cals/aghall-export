﻿using CALS.AgHallExport.Web.Helpers;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;

namespace CALS.AgHallExport.Web
{
    /// <summary>
    /// Generates an <see cref="AppConfig"/> from <see cref="IConfiguration"/> and <see cref="IWebHostEnvironment"/>
    /// </summary>
    public static class AppConfigGenerator
    {
        /// <summary>
        /// Creates an <see cref="AppConfig"/> from the <see cref="IConfiguration"/> and <see cref="IWebHostEnvironment"/>
        /// </summary>
        /// <param name="configuration"></param>
        /// <param name="environment"></param>
        /// <param name="appConfigSection">name of the section in <see cref="IConfiguration"/> where the app config resides</param>
        /// <param name="environmentBranding">Determines if the environment name will be prefixed on the AppConfig's branding</param>
        /// <returns></returns>
        public static AppConfig Create(IConfiguration configuration, IWebHostEnvironment environment, string appConfigSection = "AppConfig", bool environmentBranding = true)
        {
            var appConfig = new AppConfig();
            configuration.GetSection(appConfigSection).Bind(appConfig);

            if (environmentBranding)
            {
                if (environment.IsDevelopment())
                    appConfig.Branding = "**DEV** " + appConfig.Branding;

                else if (!environment.IsProduction())
                    appConfig.Branding = $"**{environment.EnvironmentName.ToUpper()}** {appConfig.Branding}";
            }

            return appConfig;
        }
    }
}
