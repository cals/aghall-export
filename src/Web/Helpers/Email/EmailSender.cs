﻿using System;
using System.Threading.Tasks;
using MailKit.Net.Smtp;
using MimeKit;

namespace AAE.Helpers.Email
{
    public class EmailSender : IEmailSender
    {
        private readonly EmailConfig _emailSettings;

        public EmailSender(EmailConfig emailSettings)
        {
            _emailSettings = emailSettings;
        }

        /// <inheritdoc />
        public async Task SendAdminEmailAsync(string subject, MimeEntity body)
        {
            try
            {
                var mimeMessage = new MimeMessage();
                mimeMessage.From.Add(new MailboxAddress(_emailSettings.SenderName, _emailSettings.Sender));
                mimeMessage.To.Add(new MailboxAddress(_emailSettings.SysAdminEmail, _emailSettings.SysAdminEmail));

                mimeMessage.Subject = subject;

                mimeMessage.Body = body;

                using (var client = new SmtpClient())
                {
                    // For demo-purposes, accept all SSL certificates (in case the server supports STARTTLS)
                    client.ServerCertificateValidationCallback = (s, c, h, e) => true;

                    await client.ConnectAsync(_emailSettings.MailServer);

                    // Note: only needed if the SMTP server requires authentication
                    //await client.AuthenticateAsync(_emailSettings.Sender, _emailSettings.Password);

                    await client.SendAsync(mimeMessage);

                    await client.DisconnectAsync(true);
                }
            }
            catch (Exception ex)
            {
                // TODO: handle exception
                throw new InvalidOperationException(ex.Message);
            }
        }

        /// <inheritdoc />
        public async Task SendAdminEmailAsync(string subject, string textMessage, string htmlMessage = "")
        {
            try
            {
                var mimeMessage = new MimeMessage();
                mimeMessage.From.Add(new MailboxAddress(_emailSettings.SenderName, _emailSettings.Sender));
                mimeMessage.To.Add(new MailboxAddress(_emailSettings.SysAdminEmail, _emailSettings.SysAdminEmail));

                mimeMessage.Subject = subject;

                var builder = new BodyBuilder();

                builder.TextBody = textMessage;
                if (!string.IsNullOrEmpty(htmlMessage))
                    builder.HtmlBody = htmlMessage;

                mimeMessage.Body = builder.ToMessageBody();

                using (var client = new SmtpClient())
                {
                    // For demo-purposes, accept all SSL certificates (in case the server supports STARTTLS)
                    client.ServerCertificateValidationCallback = (s, c, h, e) => true;

                    await client.ConnectAsync(_emailSettings.MailServer);

                    // Note: only needed if the SMTP server requires authentication
                    //await client.AuthenticateAsync(_emailSettings.Sender, _emailSettings.Password);

                    await client.SendAsync(mimeMessage);

                    await client.DisconnectAsync(true);
                }
            }
            catch (Exception ex)
            {
                // TODO: handle exception
                throw new InvalidOperationException(ex.Message);
            }
        }
    }
}
