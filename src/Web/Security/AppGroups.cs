﻿namespace CALS.AgHallExport.Web.Security
{
    /// <summary>
    /// System groups
    /// </summary>
    public class AppGroups
    {
        public AppGroups(string sysadmin, string users)
        {
            SysAdmin = sysadmin;
            Users = users;
        }

        public string SysAdmin { get; private set; }
        public string Users { get; private set; }
    }
}
