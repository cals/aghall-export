﻿namespace CALS.AgHallExport.Web.Authorization
{
    /// <summary>
    /// Config object holding values for app groups from appsettings
    /// </summary>
    public class AppGroupConfig
    {
        public string SysAdmin { get; set; }
        public string Users { get; set; }
    }
}
