﻿using Microsoft.AspNetCore.Authorization;

namespace CALS.AgHallExport.Web.Authorization
{
    /// <summary>
    /// Permission to simply access the app (minimum permission
    /// </summary>
    public class AccessAppPermission : IAuthorizationRequirement
    {
    }
}
