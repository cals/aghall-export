﻿using System;
using CALS.AgHallExport.Web.Security;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.Extensions.DependencyInjection;

namespace CALS.AgHallExport.Web.Authorization
{
    public static class AuthorizationExtensions
    {
        /// <summary>
        /// Adds authorization services to the specified <see cref="IServiceCollection" />. 
        /// </summary>
        /// <param name="services">The <see cref="IServiceCollection" /> to add services to.</param>
        /// <returns>The <see cref="IServiceCollection"/> so that additional calls can be chained.</returns>
        public static IServiceCollection AddPermissionsAuthorization(this IServiceCollection services)
        {
            if (services == null)
            {
                throw new ArgumentNullException(nameof(services));
            }

            services.AddAuthorization(options =>
            {
                //options.AddPolicy(Policies.PolicyName, policy => policy.Requirements.Add(new SomePermission()));
            });

            //services.AddSingleton<IAuthorizationHandler, SomePermissionHandler>();
            services.AddSingleton<IAuthorizationHandler, AccessAppHandler>();

            return services;
        }

        /// <summary>
        /// Adds authorization services to the specified <see cref="IServiceCollection" />. Also initializes the app groups
        /// </summary>
        /// <param name="services">The <see cref="IServiceCollection" /> to add services to.</param>
        /// <param name="appGroupConfig"></param>
        /// <returns>The <see cref="IServiceCollection"/> so that additional calls can be chained.</returns>
        public static IServiceCollection AddAuthorization(this IServiceCollection services, AppGroupConfig appGroupConfig)
        {
            var app_groups = new AppGroups(
                sysadmin: appGroupConfig.SysAdmin,
                users: appGroupConfig.Users);
            services.AddSingleton(app_groups);

            services.AddPermissionsAuthorization();

            services.Configure<MvcOptions>(options =>
            {
                // Global authorization
                var policy = new AuthorizationPolicyBuilder()
                    .AddRequirements(new AccessAppPermission())
                    .Build();
                options.Filters.Add(new AuthorizeFilter(policy));
            });

            return services;
        }
    }
}
