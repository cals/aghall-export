﻿using System.Threading.Tasks;
using CALS.AgHallExport.Identity;
using CALS.AgHallExport.Web.Security;
using Microsoft.AspNetCore.Authorization;

namespace CALS.AgHallExport.Web.Authorization
{
    /// <summary>
    /// Handler for <see cref="AccessAppPermission"/>
    /// </summary>
    public class AccessAppHandler : AuthorizationHandler<AccessAppPermission>
    {
        private readonly AppGroups _appGroups;

        public AccessAppHandler(AppGroups appGroups)
        {
            _appGroups = appGroups;
        }

        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, AccessAppPermission requirement)
        {
            var user = AgHallExportUser.UserFromPrincipal(context.User);

            // get the type of the AppGroups
            var appGroupType = _appGroups.GetType();

            var propertyInfo = appGroupType.GetProperties();

            // now loop all properties and get the value, checking if the user is a member
            foreach (var property in propertyInfo)
            {
                var groupName = property.GetValue(_appGroups).ToString();
                if (user.IsMemberOf(groupName))
                {
                    context.Succeed(requirement);
                    break;
                }
            }

            return Task.CompletedTask;

        }
    }
}
