﻿using System;
using System.Threading.Tasks;
using AAE.Authentication;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CALS.AgHallExport.Web.Shared.Components.MainMenu
{
    public class MainMenuViewComponent : ViewComponent
    {
        private readonly IHttpContextAccessor _contextAccessor;
        public MainMenuViewComponent(IHttpContextAccessor contextAccessor)
        {
            _contextAccessor = contextAccessor ?? throw new ArgumentNullException(nameof(contextAccessor));
        }
        public async Task<IViewComponentResult> InvokeAsync()
        {
            // // EXAMPLE below if using an app-specific identity rather than the original/standard
            // var user = new CCLogUser(_contextAccessor.HttpContext.User.Identities.Where(i=>i.AuthenticationType == ThisAppUser.ThisAppAuthenticationType).FirstOrDefault());
            var user = new ClaimsApplicationUser(_contextAccessor.HttpContext.User);



            // wrapping it in an async call until there is something that actually async (like a DB call)
            return await Task.Run(() =>
            {
                var vm = new ViewModels.MainMenu()
                {
                    LoggedInUser = $"{user.FirstName} {user.LastName}"
                };

                return View(vm);
            });
        }
    }
}
