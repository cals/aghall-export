﻿using System.Security.Cryptography.X509Certificates;
using AAE.Caching;
using AAE.Helpers.Email;
using AAE.Helpers.Errors;
using Azure.Identity;
using CALS.AgHallExport;
using CALS.AgHallExport.Web;
using CALS.AgHallExport.Web.Authorization;
using CALS.AgHallExport.Web.Helpers;
using CALS.AgHallExport.Web.Identity;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.AspNetCore.Mvc.Routing;
using UW.AspNetCore.Authentication;
using UW.Shibboleth;

var builder = WebApplication.CreateBuilder(args);

ConfigureAzureKeyVault(builder);

ConfigureAuthentication(builder, "ewdieckman");

var appConfig = AppConfigGenerator.Create(builder.Configuration, builder.Environment);
ConfigureViews(builder.Services, appConfig);


builder.Services.AddInMemoryCaching();

builder.Services.AddHttpContextAccessor();

// add in app services from the libraries
builder.Services.AddAppServices(options =>
{
    options.ConnectionString = builder.Configuration.GetConnectionString("epm");
});



// used by tag helpers for resolving virtual paths:   ~/file.jpg
builder.Services
    .AddSingleton<IActionContextAccessor, ActionContextAccessor>()
    .AddScoped(x => x
        .GetRequiredService<IUrlHelperFactory>()
        .GetUrlHelper(x.GetRequiredService<IActionContextAccessor>().ActionContext));

// ############# AUTHORIZATION ################
builder.Services.AddAuthorization(appConfig.Groups);

builder.Services.AddSingleton(appConfig.EmailSettings);
builder.Services.AddSingleton<IEmailSender, EmailSender>();

var app = builder.Build();

app.UseStaticFiles();

if (!app.Environment.IsProduction())
{
    app.UseDeveloperExceptionPage();
    app.UseStatusCodePages();
}
else
{
    app.UseHsts();
    app.UseExceptionHandler("/error");
    app.UseStatusCodePagesWithReExecute("/error/{0}");
    app.UseExceptionEmailer();
}

// AFTER UseStaticFiles, UseExceptionHandler or UseStatusCodePagesWithReExecute but BEFORE UseAuthentication and UseAuthorization
app.UseRouting();

app.UseAuthentication();
app.UseHttpsRedirection();
app.UseResponseCompression();

app.UseAgHallExportIdentity();

app.UseAuthorization();

app.UseEndpoints(endpoints =>
{
    endpoints.MapControllers();
    endpoints.MapControllerRoute("default", "{controller=Home}/{action=Index}/{id?}");
});

app.Run();

/// <summary>
/// Configures the Azure Key Vault for this app
/// </summary>
/// <param name="builder"></param>
/// <exception cref="Exception"></exception>
void ConfigureAzureKeyVault(WebApplicationBuilder builder)
{
    if (!builder.Environment.IsDevelopment())
    {
        using var x509Store = new X509Store(StoreLocation.LocalMachine);

        x509Store.Open(OpenFlags.ReadOnly);

        var certs = x509Store.Certificates;
        var distinguishedName = new X500DistinguishedName(builder.Configuration["Azure:KeyVault:SubjectDistinguishedName"]);
        var certFound = certs
            .Find(
                X509FindType.FindBySubjectDistinguishedName,
                distinguishedName.Name,
                validOnly: false)
            .OfType<X509Certificate2>();

        if (!certFound.Any())
            throw new Exception("Unable to find the certificate to authenticate and access Azure Key Vault");

        builder.Configuration.AddAzureKeyVault(
            new Uri(builder.Configuration["Azure:KeyVault:Endpoint"]),
            new ClientCertificateCredential(
                builder.Configuration["Azure:AD:DirectoryId"],
                builder.Configuration["Azure:AD:ApplicationId"],
                certFound.Single()));
    }
}

/// <summary>
/// Configures authentication for the app
/// </summary>
/// <param name="builder"></param>
/// <param name="devAuthenticationUsername"></param>
void ConfigureAuthentication(WebApplicationBuilder builder, string devAuthenticationUsername)
{
    if (!builder.Environment.IsDevelopment())
    {
        //authentication with Shibboleth
        builder.Services.AddAuthentication(options =>
        {
            options.DefaultScheme = ShibbolethAuthenticationDefaults.AuthenticationScheme;
        }).AddUWShibboleth();
    }
    else
    {
        //authenticate with local devauth
        builder.Services.AddAppDevAuthentication(devAuthenticationUsername);
    }
}

/// <summary>
/// Add controllers and views with the app config filter.  Sets defaults for routing options
/// </summary>
/// <param name="services"></param>
/// <param name="appConfig"></param>
void ConfigureViews(IServiceCollection services, AppConfig appConfig)
{
    services.AddControllersWithViews(options =>
    {
        options.Filters.Add(new AppConfigActionFilter(appConfig));
    });

    services.Configure<RouteOptions>(options => options.LowercaseUrls = true);
    services.Configure<RouteOptions>(options => options.AppendTrailingSlash = true);

    services.AddResponseCompression(options => options.EnableForHttps = true);

}
