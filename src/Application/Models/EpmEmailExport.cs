using CALS.AgHallExport.Application.Interfaces;

namespace CALS.AgHallExport.Application.Models
{
    public class EpmEmailExport : ILyrisImport, IGGToolImport
    {
        public string Email { get; set; }
        public string Name { get; set; }
        public string FullName { get => Name; set => Name = value; }
        public string EmailAddress { get => Email; set => Email = value; }
        public string SubType { get; set; } = "mail";
        public string MemberType { get; set; } = "normal";

        public string IsListAdmin { get; set; } = "F";

        string IGGToolImport.EmailAddress { get => Email; set => Email = value; }
        string IGGToolImport.Role { get; set; } = "MEMBER";
        string IGGToolImport.DeliverySettings { get; set; } = "ALL_MAIL";

    }
}
