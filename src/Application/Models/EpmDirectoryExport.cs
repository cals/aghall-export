using CALS.AgHallExport.Application.Interfaces;

namespace CALS.AgHallExport.Application.Models
{
    public class EpmDirectoryExport
    {
        public string Department { get; set; }
        public string Name { get; set; }
        

    }
}
