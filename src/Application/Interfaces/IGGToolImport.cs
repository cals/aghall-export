using System;
using System.Collections.Generic;
using System.Text;

namespace CALS.AgHallExport.Application.Interfaces
{
    /// <summary>
    /// Interface to format record for import using GGTool
    /// </summary>
    public interface IGGToolImport
    {
        string EmailAddress { get; set; }
        string DeliverySettings { get; set; }
        string Role { get; set; }
    }
}
