﻿using CALS.AgHallExport.Application.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CALS.AgHallExport.Interfaces
{
    public interface IGGToolImportCreator
    {
        /// <summary>
        /// Create a CSV file with the specified members
        /// </summary>
        Task<byte[]> CreateCSV(IEnumerable<IGGToolImport> members);
    }
}