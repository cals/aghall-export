namespace CALS.AgHallExport.Application.Interfaces
{
    /// <summary>
    /// Interface for Lyris import records
    /// </summary>
    public interface ILyrisImport
    {
        string FullName { get; set; }
        string EmailAddress { get; set; }
        string SubType { get; set; }
        string MemberType { get; set; }
        string IsListAdmin { get; set; }
    }
}
