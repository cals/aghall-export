﻿using CALS.AgHallExport.Application.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CALS.AgHallExport.Interfaces
{
    public interface IEpmExportQueries
    {
        /// <summary>
        /// return all Ag Hall members email
        /// </summary>
        Task<IList<EpmEmailExport>> GetEmailAsync();

        /// <summary>
        /// return all Ag Hall members name and dept
        /// </summary>
        Task<IList<EpmDirectoryExport>> GetDirectoryAsync();
    }
}