using CALS.AgHallExport.Application.Interfaces;
using CsvHelper.Configuration;

namespace CALS.AgHallExport.Application.GGTool
{
    public class IGGToolImportMap : ClassMap<IGGToolImport>
    {
        public IGGToolImportMap()
        {
            Map(m => m.EmailAddress).Index(0).Name("Member");
            Map(m => m.Role).Index(1).Name("Role");
            Map(m => m.DeliverySettings).Index(2).Name("DeliverySettings");

        }
    }
}
