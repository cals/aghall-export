using CALS.AgHallExport.Application.Interfaces;
using CALS.AgHallExport.Interfaces;
using CsvHelper;
using CsvHelper.Configuration;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Threading.Tasks;

namespace CALS.AgHallExport.Application.GGTool
{
    /// <summary>
    /// Class responsible for creating CSV import files for Lyris
    /// </summary>
    public class GGToolImportCreator : IGGToolImportCreator
    {
        /// <inheritdoc />
        public async Task<byte[]> CreateCSV(IEnumerable<IGGToolImport> members)
        {
            using (var memoryStream = new MemoryStream())
            {
                using (var writer = new StreamWriter(memoryStream))
                {
                    using (var csv = new CsvWriter(writer, CultureInfo.InvariantCulture))
                    {
                        var config = new CsvConfiguration(CultureInfo.InvariantCulture)
                        {
                            Delimiter = ",",
                            HasHeaderRecord = true
                        };
                        csv.Context.RegisterClassMap<IGGToolImportMap>();


                        csv.WriteRecords(members);

                        await writer.FlushAsync();
                    }


                }

                return memoryStream.ToArray();
            }
        }
    }
}
