using System;
using System.IO;
using System.Reflection;
using System.Threading.Tasks;

namespace CALS.AgHallExport.Data
{
    /// <summary>
    /// Used for reading embedded resources from class library
    /// </summary>
    public static class EmbeddedResource
    {
        private static StreamReader GetStream(Assembly assembly, string name)
        {
            foreach (string resName in assembly.GetManifestResourceNames())
            {
                if (resName.EndsWith($".{name}"))
                    return new StreamReader(assembly.GetManifestResourceStream(resName));
            }

            return null;
        }

        private async static Task<string> GetStringAsync(Assembly assembly, string name)
        {
            StreamReader sr = GetStream(assembly, name);
            var data = string.Empty;
            if (sr == null)
                throw new Exception($"Error reading {name}");

            try
            {
                data = await sr.ReadToEndAsync();
            }
            catch (Exception ex)
            {
                throw new Exception($"Missing resource: {name}", ex);
            }
            finally
            {
                sr.Close();
            }

            return data;
        }

        /// <summary>
        /// Returns a string that has been marked as an embedded resource in this assembly
        /// </summary>
        public async static Task<string> GetStringAsync(string name)
        {
            return await GetStringAsync(typeof(EmbeddedResource).Assembly, name);
        }
    }
}
