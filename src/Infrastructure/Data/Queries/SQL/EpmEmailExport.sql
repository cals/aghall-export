SELECT DISTINCT 
    addr.uw_bn_email_addr email
    ,names.name_display name
    FROM 
        PS_UW_HR_CURJOB_VW cj
    INNER JOIN
        PS_UW_HR_ADRBUS_VW addr ON cj.emplid=addr.emplid
    INNER JOIN
        PS_UW_P_NAMES_VW names ON cj.emplid=names.emplid
        
        
    WHERE
        --grab employees with an end AFTER today or have no end date
         (cj.uw_job_end_dt > CURRENT_DATE
                OR cj.uw_job_end_dt IS NULL) 

        AND 
        (
            --grab employees that are NOT student hourlies AND have a business address location of Ag Hall (A0070)
            (addr.location = 'A0070' AND cj.empl_class<>'SH')
            
            OR
            
            --grab student hourlies that have a work location (HRS) of Ag Hall (A0070)
            (cj.location = 'A0070' AND cj.empl_class='SH' )
        )
        
        --remove any with blank emails
        AND TRIM(addr.uw_bn_email_addr) IS NOT NULL
        
    ORDER BY names.name_display