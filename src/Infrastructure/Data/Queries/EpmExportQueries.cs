using AAE.Data.Interfaces;
using CALS.AgHallExport.Application.Models;
using CALS.AgHallExport.Data;
using CALS.AgHallExport.Interfaces;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CALS.AgHallExport.Infrastructure.Data.Queries
{
    public class EpmExportQueries : IEpmExportQueries
    {
        private readonly IDbConnectionFactory _dbConnectionFactory;
        private const string EpmEmailExportFilename = "EpmEmailExport.sql";
        private const string EpmDirectoryExportFilename = "EpmDirectoryExport.sql";

        public EpmExportQueries(IDbConnectionFactoryEpm dbConnectionFactory)
        {
            _dbConnectionFactory = dbConnectionFactory ?? throw new ArgumentNullException(nameof(dbConnectionFactory));
        }

        /// <inheritdoc />
        public async Task<IList<EpmEmailExport>> GetEmailAsync()
        {
            using (var connection = _dbConnectionFactory.GetDbConnection())
            {
                var sql = await EmbeddedResource.GetStringAsync(EpmEmailExportFilename);

                var result = await connection.QueryAsync<EpmEmailExport>(sql).ConfigureAwait(false);

                return result.ToList();
            }
        }

        public async Task<IList<EpmDirectoryExport>> GetDirectoryAsync()
        {
            using (var connection = _dbConnectionFactory.GetDbConnection())
            {
                var sql = await EmbeddedResource.GetStringAsync(EpmDirectoryExportFilename);

                var result = await connection.QueryAsync<EpmDirectoryExport>(sql).ConfigureAwait(false);

                return result.ToList();
            }
        }
    }
}
