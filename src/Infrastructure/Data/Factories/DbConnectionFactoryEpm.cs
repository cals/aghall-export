﻿using AAE.Data.Factories;
using CALS.AgHallExport.Interfaces;

namespace CALS.AgHallExport.Data.Factories
{
    /// <summary>
    /// Connection factory for EPM database
    /// </summary>
    public class DbConnectionFactoryEpm : OracleConnectionFactory, IDbConnectionFactoryEpm
    {
        public DbConnectionFactoryEpm(string connectionString) : base(connectionString)
        {

        }
    }
}
