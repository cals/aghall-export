﻿using AAE.Data.Interfaces;
using System.Data;

namespace AAE.Data.Factories
{
    /// <summary>
    /// Base connection factory class
    /// </summary>
    public abstract class DbConnectionFactory : IDbConnectionFactory
    {

        private readonly string _connectionString;
        public DbConnectionFactory(string connectionString)
        {
            _connectionString = connectionString;
        }


        public IDbConnection GetDbConnection()
        {
            return GetConnection(_connectionString);
        }

        /// <summary>
        /// Returns the implemented class of <see cref="IDbConnection"/>
        /// </summary>
        /// <param name="connectionString">Optional connection string to initialize the connection object</param>
        protected abstract IDbConnection GetConnection(string connectionString = "");

        /// <inheritdoc />
        public abstract IDbDataParameter GetDbParameter();

        /// <inheritdoc />
        public abstract IDbDataParameter GetDbParameter(string parameterName, DbType dbType);

        /// <inheritdoc />
        public abstract IDbDataParameter GetDbParameter(string parameterName, object value);

        /// <inheritdoc />
        public abstract IDbDataParameter GetDbParameter(string parameterName, DbType dbType, int size);

    }
}
