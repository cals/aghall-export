using Oracle.ManagedDataAccess.Client;
using System.Data;

namespace AAE.Data.Factories
{
    /// <summary>
    /// Implementation of <see cref="DbConnectionFactory"/> for Oracle
    /// </summary>
    public class OracleConnectionFactory : DbConnectionFactory
    {
        public OracleConnectionFactory(string connectionString) : base(connectionString)
        {

        }

        public override IDbDataParameter GetDbParameter()
        {
            return new OracleParameter();
        }

        public override IDbDataParameter GetDbParameter(string parameterName, DbType dbType)
        {
            var param = new OracleParameter();
            param.DbType = dbType;
            param.ParameterName = parameterName;
            return param;
        }

        public override IDbDataParameter GetDbParameter(string parameterName, object value)
        {
            return new OracleParameter(parameterName, value);
        }

        public override IDbDataParameter GetDbParameter(string parameterName, DbType dbType, int size)
        {
            var param = new OracleParameter();
            param.DbType = dbType;
            param.ParameterName = parameterName;
            param.Size = size;
            return param;
        }

        protected override IDbConnection GetConnection(string connection_string = "")
        {
            return new OracleConnection(connection_string);
        }
    }
}
