﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CALS.AgHallExport.Infrastructure.Exceptions
{
    public class ConnectionClosedException : Exception
    {
        public ConnectionClosedException() : base("Open connection is needed.  Closed connection provided.")
        {

        }
    }
}
